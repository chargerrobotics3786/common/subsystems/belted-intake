package com.chargerrobotics.common.subsystem.belted_intake;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.chargerrobotics.common.subsystem.belted_intake.BeltedIntakeConfiguration.BeltedIntakeConfigurationBuilder;
import com.chargerrobotics.common.subsystem.belted_intake.BeltedIntakeSubsystem.BeltedIntakeSubsystemStates;
import com.revrobotics.REVLibError;
import com.revrobotics.spark.ClosedLoopSlot;
import com.revrobotics.spark.SparkBase;
import com.revrobotics.spark.SparkBase.ControlType;
import com.revrobotics.spark.SparkClosedLoopController;
import com.revrobotics.spark.SparkFlex;
import com.revrobotics.spark.SparkMax;
import com.revrobotics.spark.SparkRelativeEncoder;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.wpilibj.DigitalInput;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BeltedIntakeSubsystemTest {

  double speedForState;
  double intakeSpeed = 6979.0;
  double ejectSpeed = -3786.0;
  double carryPosition = 2.0;
  double stopSpeed = 0.0;
  @Mock SparkMax beltedIntakeMotor;
  @Mock DigitalInput beltedIntakeSensor;
  @Mock SparkClosedLoopController closedLoopController;
  Debouncer beltedIntakeSensorDebouncer = new Debouncer(0);
  @Mock SparkRelativeEncoder beltedIntakeRelativeEncoder;

  BeltedIntakeSubsystem beltedIntakeSubsystem;
  @InjectMocks BeltedIntakeConfiguration configuration;

  @BeforeEach
  void testInit() {
    configuration =
        BeltedIntakeConfiguration.builder()
            .motor(beltedIntakeMotor)
            .sensor(beltedIntakeSensor)
            .build();
    beltedIntakeSubsystem =
        BeltedIntakeSubsystem.builder()
            .configuration(configuration)
            .stateValue(BeltedIntakeSubsystemStates.INTAKE, intakeSpeed)
            .stateValue(BeltedIntakeSubsystemStates.EJECT, ejectSpeed)
            .stateValue(BeltedIntakeSubsystemStates.CARRY, carryPosition)
            .stateValue(BeltedIntakeSubsystemStates.STOPPED, stopSpeed)
            .build();
  }

  @ParameterizedTest(name = "Given {1} motor type, attempt to update state to {0}")
  @MethodSource("provideParameters")
  void givenState_whenSetDesiredState_returnsUpdatedCurrentState(
      BeltedIntakeSubsystemStates state, SparkBase motor) {
    if (state == BeltedIntakeSubsystemStates.CHANGING_STATE) {
      beltedIntakeSubsystem.setDesiredState(state);
      assertNotEquals(state, beltedIntakeSubsystem.getCurrentState());
      return; // Needed in order for speedForState not be able to attempt reading the speed of this
      // state, as it doesn't have one
    }
    speedForState = beltedIntakeSubsystem.getStateValue(state);
    when(motor.getEncoder()).thenReturn(beltedIntakeRelativeEncoder);
    when(beltedIntakeRelativeEncoder.getVelocity()).thenReturn(speedForState);
    when(motor.getClosedLoopController()).thenReturn(closedLoopController);
    when(closedLoopController.setReference(
            speedForState, ControlType.kVelocity, ClosedLoopSlot.kSlot0))
        .thenReturn(REVLibError.kOk);
    configuration =
        BeltedIntakeConfiguration.builder().motor(motor).sensor(beltedIntakeSensor).build();
    beltedIntakeSubsystem =
        BeltedIntakeSubsystem.builder()
            .configuration(configuration)
            .stateValue(BeltedIntakeSubsystemStates.INTAKE, intakeSpeed)
            .stateValue(BeltedIntakeSubsystemStates.EJECT, ejectSpeed)
            .stateValue(BeltedIntakeSubsystemStates.CARRY, carryPosition)
            .stateValue(BeltedIntakeSubsystemStates.STOPPED, stopSpeed)
            .build();
    beltedIntakeSubsystem.setDesiredState(state);
    beltedIntakeSubsystem.periodic();
    verify(beltedIntakeRelativeEncoder).getVelocity();
    assertEquals(
        speedForState,
        beltedIntakeSubsystem.getCurrentMotorSpeed(),
        configuration.getVelocityStateToleranceRevolutionsPerMinute());
    assertEquals(state, beltedIntakeSubsystem.getCurrentState());
  }

  @Test
  void whenGetSensor_returnsFalse() {
    when(beltedIntakeSensor.get()).thenReturn(false);
    assertEquals(false, beltedIntakeSubsystem.getSensorState());
    verify(beltedIntakeSensor).get();
  }

  @Test
  void whenGetSensor_returnsDebouncedState() {
    configuration =
        BeltedIntakeConfiguration.builder()
            .motor(beltedIntakeMotor)
            .sensor(beltedIntakeSensor)
            .debouncer(beltedIntakeSensorDebouncer)
            .build();
    beltedIntakeSubsystem =
        BeltedIntakeSubsystem.builder()
            .configuration(configuration)
            .stateValue(BeltedIntakeSubsystemStates.INTAKE, intakeSpeed)
            .stateValue(BeltedIntakeSubsystemStates.EJECT, ejectSpeed)
            .stateValue(BeltedIntakeSubsystemStates.CARRY, carryPosition)
            .stateValue(BeltedIntakeSubsystemStates.STOPPED, stopSpeed)
            .build();

    when(beltedIntakeSensorDebouncer.calculate(beltedIntakeSensor.get())).thenReturn(true);
    assertEquals(true, beltedIntakeSubsystem.getSensorState());

    when(beltedIntakeSensorDebouncer.calculate(beltedIntakeSensor.get())).thenReturn(false);
    assertEquals(false, beltedIntakeSubsystem.getSensorState());
  }

  @Test
  void givenNoMotor_whenBuild_ThrowsNullPointerException() {
    BeltedIntakeConfigurationBuilder unbuiltBeltedIntakeConfiguration =
        BeltedIntakeConfiguration.builder();
    assertThrows(NullPointerException.class, unbuiltBeltedIntakeConfiguration::build);
  }

  @Test
  void givenNoSensor_whenGetSensor_returnsNullAndNoInteraction() {
    configuration = BeltedIntakeConfiguration.builder().motor(beltedIntakeMotor).build();

    beltedIntakeSubsystem =
        BeltedIntakeSubsystem.builder()
            .configuration(configuration)
            .stateValue(BeltedIntakeSubsystemStates.INTAKE, intakeSpeed)
            .stateValue(BeltedIntakeSubsystemStates.EJECT, ejectSpeed)
            .stateValue(BeltedIntakeSubsystemStates.CARRY, carryPosition)
            .stateValue(BeltedIntakeSubsystemStates.STOPPED, stopSpeed)
            .build();

    assertEquals(null, beltedIntakeSubsystem.getSensorState());
    verifyNoInteractions(beltedIntakeSensor);
  }

  @Test
  void givenMissingSpeedValues_whenBuild_throwsIllegalArgumentException() {
    configuration = BeltedIntakeConfiguration.builder().motor(beltedIntakeMotor).build();
    BeltedIntakeSubsystem.BeltedIntakeSubsystemBuilder unbuiltBeltedIntakeSubsystem =
        BeltedIntakeSubsystem.builder().configuration(configuration);

    assertThrows(IllegalArgumentException.class, unbuiltBeltedIntakeSubsystem::build);
  }

  @Test
  void whenSetDesiredState_returnsChangingState() {
    double currentMotorVelocityRevolutionsPerMinute = intakeSpeed - 500;
    when(beltedIntakeMotor.getEncoder()).thenReturn(beltedIntakeRelativeEncoder);
    when(beltedIntakeRelativeEncoder.getVelocity())
        .thenReturn(currentMotorVelocityRevolutionsPerMinute);
    when(beltedIntakeMotor.getClosedLoopController()).thenReturn(closedLoopController);
    when(closedLoopController.setReference(
            intakeSpeed, ControlType.kVelocity, ClosedLoopSlot.kSlot0))
        .thenReturn(REVLibError.kOk);
    beltedIntakeSubsystem.setDesiredState(BeltedIntakeSubsystemStates.INTAKE);
    beltedIntakeSubsystem.periodic();
    assertEquals(
        BeltedIntakeSubsystemStates.CHANGING_STATE, beltedIntakeSubsystem.getCurrentState());
  }

  private static Stream<Arguments> provideParameters() {
    SparkFlex flexMotor = mock(SparkFlex.class);
    SparkMax maxMotor = mock(SparkMax.class);

    return Stream.of(
        Arguments.of(BeltedIntakeSubsystemStates.INTAKE, flexMotor),
        Arguments.of(BeltedIntakeSubsystemStates.EJECT, flexMotor),
        Arguments.of(BeltedIntakeSubsystemStates.STOPPED, flexMotor),
        Arguments.of(BeltedIntakeSubsystemStates.CHANGING_STATE, flexMotor),
        Arguments.of(BeltedIntakeSubsystemStates.INTAKE, maxMotor),
        Arguments.of(BeltedIntakeSubsystemStates.EJECT, maxMotor),
        Arguments.of(BeltedIntakeSubsystemStates.STOPPED, maxMotor),
        Arguments.of(BeltedIntakeSubsystemStates.CHANGING_STATE, maxMotor));
  }

  @Test
  void testInvertedMotor() {
    when(beltedIntakeMotor.getClosedLoopController()).thenReturn(closedLoopController);
    when(beltedIntakeMotor.getEncoder()).thenReturn(beltedIntakeRelativeEncoder);
    beltedIntakeSubsystem.setInvertedState(true);
    beltedIntakeSubsystem.setDesiredState(BeltedIntakeSubsystemStates.INTAKE);
    beltedIntakeSubsystem.periodic();
    verify(closedLoopController)
        .setReference(-intakeSpeed, ControlType.kVelocity, ClosedLoopSlot.kSlot0);
    beltedIntakeSubsystem.setInvertedState(false);
    beltedIntakeSubsystem.setDesiredState(BeltedIntakeSubsystemStates.INTAKE);
    beltedIntakeSubsystem.periodic();
    verify(closedLoopController)
        .setReference(intakeSpeed, ControlType.kVelocity, ClosedLoopSlot.kSlot0);
  }

  @Test
  void carryStateTest() {
    when(beltedIntakeMotor.getClosedLoopController()).thenReturn(closedLoopController);
    when(beltedIntakeMotor.getEncoder()).thenReturn(beltedIntakeRelativeEncoder);
    beltedIntakeSubsystem.setDesiredState(BeltedIntakeSubsystemStates.CARRY);
    beltedIntakeSubsystem.periodic();
    verify(closedLoopController)
        .setReference(
            beltedIntakeSubsystem.getStateValue(BeltedIntakeSubsystemStates.CARRY),
            ControlType.kPosition,
            ClosedLoopSlot.kSlot1);
  }
}
