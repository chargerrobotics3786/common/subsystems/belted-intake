package com.chargerrobotics.common.subsystem.belted_intake;

import com.revrobotics.spark.SparkBase;
import edu.wpi.first.math.filter.Debouncer;
import edu.wpi.first.wpilibj.DigitalInput;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * A Class that provides the BeltedIntakeSubsystem crucial information that it requires in order to
 * operate
 */
@Builder
public class BeltedIntakeConfiguration {
  @Getter @NonNull private SparkBase motor;
  @Getter private DigitalInput sensor;
  @Getter private Debouncer debouncer;
  @Getter private Double debounceTimeSeconds;
  @Getter @Builder.Default private Double velocityStateToleranceRevolutionsPerMinute = 100.0;
}
