package com.chargerrobotics.common.subsystem.belted_intake;

import com.revrobotics.spark.ClosedLoopSlot;
import com.revrobotics.spark.SparkBase.ControlType;
import com.revrobotics.spark.config.EncoderConfig;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import java.util.EnumSet;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;

/**
 * A Subsystem that handles the creation and management of the Belted Intake, provides various
 * methods in order to set and get this Subsystem's state.
 */
@Slf4j
public class BeltedIntakeSubsystem extends SubsystemBase {
  /** The Allowed States of the {@link BeltedIntakeSubsystem} */
  public enum BeltedIntakeSubsystemStates {
    /** The state to stop intaking/ejecting an object */
    STOPPED,
    /** The state to intake an object */
    INTAKE,
    /** The state to hold the position of an object */
    CARRY,
    /** The state to eject an object */
    EJECT,
    /**
     * A transitional state between {@linkplain BeltedIntakeSubsystemStates#INTAKE}, {@linkplain
     * BeltedIntakeSubsystemStates#EJECT}, and {@linkplain BeltedIntakeSubsystemStates#STOPPED}.
     * Cannot be directly commanded
     */
    CHANGING_STATE
  }

  private Map<BeltedIntakeSubsystemStates, Double> stateValues;
  @Getter private double currentMotorSpeed;
  @Getter private BeltedIntakeSubsystemStates currentState;
  private BeltedIntakeSubsystemStates desiredState;
  private BeltedIntakeConfiguration configuration;
  private boolean invertDirection = false;

  @Builder
  private BeltedIntakeSubsystem(
      BeltedIntakeConfiguration configuration,
      @Singular Map<BeltedIntakeSubsystemStates, Double> stateValues)
      throws IllegalArgumentException {
    this.configuration = configuration;
    this.stateValues = stateValues;

    EnumSet<BeltedIntakeSubsystemStates> commandedStates =
        EnumSet.of(
            BeltedIntakeSubsystemStates.STOPPED,
            BeltedIntakeSubsystemStates.INTAKE,
            BeltedIntakeSubsystemStates.CARRY,
            BeltedIntakeSubsystemStates.EJECT);

    for (BeltedIntakeSubsystemStates state : commandedStates) {
      if (!stateValues.containsKey(state)) {
        throw new IllegalArgumentException(
            "You have failed to provide a speed for a required state.");
      }
    }
    setDesiredState(BeltedIntakeSubsystemStates.STOPPED);
  }

  /**
   * Method to request the subsystem to switch to another state
   *
   * @param state represents the commandable state to switch to
   */
  public void setDesiredState(BeltedIntakeSubsystemStates state) {
    if (state == BeltedIntakeSubsystemStates.CHANGING_STATE) {
      log.warn("Attempted to change to a non-commandable state ({})", state);
      return;
    }
    desiredState = state;
  }

  /**
   * A method to invert the direction of the motor
   *
   * @param invertDirection true is to invert the direction of the motor and false is the
   *     normal/currnent direction
   */
  public void setInvertedState(boolean invertDirection) {
    this.invertDirection = invertDirection;
  }

  /** A method to set the motor in velocity control type */
  public void setVelocityControl() {
    configuration
        .getMotor()
        .getClosedLoopController()
        .setReference(getStateValue(desiredState), ControlType.kVelocity, ClosedLoopSlot.kSlot0);
  }

  /**
   * Method in order to update the motor's position
   *
   * @param position the position to set the motor, defaults to rotation unless {@linkplain
   *     EncoderConfig#positionConversionFactor(double)} is set otherwise
   */
  public void setMotorPosition(double position) {
    configuration.getMotor().getEncoder().setPosition(position);
  }

  /* A method to set the motor in position control type */
  public void setPositionControl() {
    configuration
        .getMotor()
        .getClosedLoopController()
        .setReference(
            getStateValue(BeltedIntakeSubsystemStates.CARRY),
            ControlType.kPosition,
            ClosedLoopSlot.kSlot1);
  }

  /**
   * A method to calculate if the current velocity is near expected velocity
   *
   * @param targetedState represent the state which needs to be calculated
   */
  private boolean isVelocityAtState(BeltedIntakeSubsystemStates targetedState) {
    return MathUtil.isNear(
        stateValues.get(targetedState),
        invertDirection ? -currentMotorSpeed : currentMotorSpeed,
        configuration.getVelocityStateToleranceRevolutionsPerMinute());
  }

  /**
   * Gets the speed assined to the specific state
   *
   * @param state represents the state to obtain the speed from. (See {@link
   *     BeltedIntakeSubsystemStates} for available states)
   * @return the speed assigned to the specific state
   */
  public double getStateValue(BeltedIntakeSubsystemStates state) {
    return invertDirection ? -stateValues.get(state) : stateValues.get(state);
  }

  /**
   * Gets the state of the belted intake sensor.
   *
   * @return the sensors state. (null if no sensor is passed on construction)
   */
  @javax.annotation.Nullable
  public Boolean getSensorState() {
    if (configuration.getSensor() == null) {
      log.warn("Attempted access to null sensor");
      return null;
    }

    if (configuration.getDebouncer() == null) {
      return configuration.getSensor().get();
    } else {
      return configuration.getDebouncer().calculate(configuration.getSensor().get());
    }
  }

  @Override
  public void periodic() {
    currentMotorSpeed = configuration.getMotor().getEncoder().getVelocity();

    if (desiredState == BeltedIntakeSubsystemStates.CARRY) {
      if (currentState != BeltedIntakeSubsystemStates.CARRY) {
        currentState = BeltedIntakeSubsystemStates.CARRY;
        setPositionControl();
      }
    } else if (isVelocityAtState(BeltedIntakeSubsystemStates.INTAKE)) {
      currentState = BeltedIntakeSubsystemStates.INTAKE;
      setVelocityControl();
    } else if (isVelocityAtState(BeltedIntakeSubsystemStates.EJECT)) {
      currentState = BeltedIntakeSubsystemStates.EJECT;
      setVelocityControl();
    } else if (isVelocityAtState(BeltedIntakeSubsystemStates.STOPPED)) {
      currentState = BeltedIntakeSubsystemStates.STOPPED;
      setVelocityControl();
    } else {
      currentState = BeltedIntakeSubsystemStates.CHANGING_STATE;
      setVelocityControl();
    }
  }
}
