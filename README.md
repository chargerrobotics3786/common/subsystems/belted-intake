# Belted Intake Subsystem Library

![coverage](https://gitlab.com/chargerrobotics3786/common/subsystems/belted-intake/badges/main/coverage.svg?job=report-coverage)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chargerrobotics3786_belted-intake&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=chargerrobotics3786_belted-intake)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=chargerrobotics3786_belted-intake&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=chargerrobotics3786_belted-intake)

## Important Versions

This library uses the below versions for key components:

- Java 17
- Gradle 8.10.2

## Description

This Library was created in order to combat the time needed to rewrite intake code every year for the FRC Build Seasons.

This code utilizes **SparkMax** Motor Controllers provided by **REV Hardware**.

This code utilizes **DigitalInput** for interfacing with the DIO Ports found on the ROBO-RIO.

The Library/Subsystem provides methods to interface with the subsystem states, sensor states, and current motor speed. (Between -1 and 1)

### Gradle

```gradle
repositories {
    maven {
        url "https://gitlab.com/api/v4/groups/chargerrobotics3786/common/-/packages/maven"
        name "Charger Robotics Common"
    }
}

dependencies {
    implementation "com.chargerrobotics.common.subsystem:belted-intake:<version>"
}
```
